var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM Station;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};
exports.getByID = function(station_id,callback) {
    var query = 'SELECT * FROM Station WHERE station_id = ?';
    var queryData = [station_id]
    connection.query(query,queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getSchedule = function(station_id,callback) {
    var query = 'CALL get_schedule(?);';
    var queryData = [station_id]
    connection.query(query,queryData, function(err, result) {
        callback(err, result);
    });
};

exports.busy = function(num_pass,callback) {
    var query = 'SELECT station_name, count(p.passenger_id) AS pass_count FROM Station s ' +
        'JOIN Passenger p ON s.station_id = p.station_id ' +
        'GROUP BY s.station_id ' +
        'HAVING count(p.passenger_id) > ?;';
    var queryData = [num_pass];
    connection.query(query,queryData, function(err, result) {
        callback(err, result);
    });
};