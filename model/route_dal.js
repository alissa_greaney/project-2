var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM Route;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.check = function(route_id,callback) {
    var query = 'SELECT DISTINCT train_id, train_type ' +
        'FROM train_routes WHERE route_id = ?';
    var queryData = [route_id];

    connection.query(query,queryData, function(err, result) {
        callback(err, result);
    });
};
