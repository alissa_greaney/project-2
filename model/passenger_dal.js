var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM Passenger;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getByID = function(passenger_id,callback) {
    var query = 'SELECT * FROM Passenger WHERE passenger_id = ?;';
    var queryData = [passenger_id];
    console.log(query);
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
exports.insert = function(params, callback) {
    var query = 'INSERT INTO Passenger (first_name, last_name,price, num_stops, station_id) VALUES (?,?,?,?,?)';
    var queryData = [params.first_name, params.last_name,0, 0, 1];

    connection.query(query, queryData, function(err,result){
        callback(err,result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE Passenger SET num_stops = ?, station_id = ? WHERE passenger_id = ?';
    var queryData = [params.num_stops, params.station_id, params.passenger_id];

    connection.query(query, queryData, function(err,result) {
        var query = 'CALL insert_num_stops(?,?)';
        var queryData = [params.passenger_id, params.num_stops];
        connection.query(query, queryData, function (err, result) {
            callback(err, result);
        });
    });
};

exports.getID = function(params,callback) {
    var query = 'SELECT passenger_id FROM Passenger WHERE first_name = ? and last_name = ?;';
    var queryData = [params.first_name, params.last_name];
    console.log(query);
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};