var express = require('express');
var router = express.Router();
var station_dal = require('../model/station_dal');

// View All companys
router.get('/all', function(req, res) {
    station_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('station/stationViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.station_id == null) {
        res.send('station_id is null');
    }
    else {
        station_dal.getSchedule(req.query.station_id,function(err,result){
            if(err){
                res.send(err);
            }
            else{
                station_dal.getByID(req.query.station_id, function(err,result2){
                    res.render('station/stationViewByID', {'result': result[0], 'station':result2[0]});
                });
            }
        });
    }
});

router.get('/choose', function(req, res) {
    res.render('station/chooseStation');
});

router.get('/busy', function(req, res) {
    station_dal.busy(req.query.num_pass,function(err,result){
        res.render('station/busyStation',{'result':result});
    })
});

module.exports = router;
