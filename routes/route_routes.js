var express = require('express');
var router = express.Router();
var route_dal = require('../model/route_dal');

// View All companys
router.get('/all', function(req, res) {
    route_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('station/stationViewAll', { 'result':result });
        }
    });

});

router.get('/choose', function(req, res) {
    res.render('route/chooseRoute');
});

router.get('/check', function(req, res) {
    route_dal.check(req.query.route_id,function(err,result){
        res.render('route/routeCheck',{'result':result});
    })
});

module.exports = router;
