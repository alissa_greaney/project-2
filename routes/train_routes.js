var express = require('express');
var router = express.Router();
var train_dal = require('../model/train_dal');

// View All companys
router.get('/all', function(req, res) {
    train_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('station/stationViewAll', { 'result':result });
        }
    });

});

module.exports = router;
