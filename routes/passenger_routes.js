var express = require('express');
var router = express.Router();
var passenger_dal = require('../model/passenger_dal');
var station_dal = require('../model/station_dal');


// View All companys
router.get('/all', function(req, res) {
    passenger_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('passenger/passengerViewAll', { 'result':result });
        }
    });

});

router.get('/register', function(req, res) {

    res.render('passenger/passengerRegister');

});

router.get('/create',function(req,res){
    passenger_dal.insert(req.query,function(err,result){
        if (err) {
            console.log(err)
            res.send(err);
        }
        else {
            var passenger_id = result.insertId;
            res.render('passenger/passengerSuccess', {'result':passenger_id});
        }
    });
});


router.get('/buy', function(req, res) {
    station_dal.getAll(function(err,result){
        if (err) {
            console.log(err)
            res.send(err);
        }
        else {
            res.render('passenger/passengerBuy', {'result': result});
        }
    });
});

router.get('/ticket', function(req, res) {
    passenger_dal.update(req.query,function(err,result){
        if(err){
            console.log(err)
            res.send(err);
        }
        else{
            passenger_dal.getByID(req.query.passenger_id,function(err,result){
                if(err){
                    res.send(err);
                }
                else{
                    res.render('passenger/ticketSuccess', {'result':result[0]})
                }
            });
        }
    });

});

router.get('/forgotID', function(req, res) {
    res.render('passenger/passengerForgotID');
});

router.get('/getID', function(req, res) {
        passenger_dal.getID(req.query,function(err,result){
            if(err){
                res.send(err);
            }
            else{
                res.render('passenger/passengerViewID', { 'result':result[0] });
            }
        });
});

module.exports = router;
